﻿namespace RPG.DirkWoodchip
{
    /// <summary>Technical names of templates specific to the Dirk Woodchip project.</summary>
    public static class Templates
    {

        // TODO: Get the technical names from the project itself somehow
        
        /// <inheritdoc cref="Templates"/>
        public enum DialogueFragments
        {
            /// <summary>No template or not a particularly important one.</summary>
            None,
            /// <summary>The default template for any unimportant NPC.</summary>
            CharacterDialogueFragment,
            /// <summary>The main playable character's dialogue.</summary>
            DirkDialogueFragment,
            /// <summary>The main supporting character's dialogue.</summary>
            ArreonDialogueFragment,
            /// <summary>Another playable character.</summary>
            AbigailDialogueFragment,
            /// <summary>Another playable character.</summary>
            LymDialogueFragment,
            /// <summary> The dialogue fragment used for one-time permanent choices. </summary>
            ChoiceDialogueFragment

        }

        /// <inheritdoc cref="Templates"/>
        public enum Hubs
        {
            /// <summary>No template or not a particularly important one.</summary>
            None,
            /// <summary>The hub marking the start of the dialogue, just before the first NPC reply. A single hub per dialogue.</summary>
            RootHub,
            /// <summary>The very last hub of a dialogue; jumping here ends it. A single hub per dialogue.</summary>
            EndHub,
            /// <summary>The hub used when meeting a character for the very first time. A single hub per dialogue.</summary>
            MeetHub,
            /// <summary> The hub used for one-time permanent choices. </summary>
            ChoiceHub,
            /// <summary> A hub indicating a change of the playable character. </summary>
            PlayerChangeHub,
            /// <summary> A hub from which a random option is chosen. </summary>
            RandomHub,

        }

        /// <inheritdoc cref="Templates"/>
        public enum Jumps
        {
            /// <summary>No template or not a particularly important one.</summary>
            None,
            /// <summary>Jumps to the <see cref="Hubs.RootHub"/> element.</summary>
            RootJump,
            /// <summary>Jumps to the <see cref="Hubs.EndHub"/> element.</summary>
            EndJump,
            /// <summary>Jumps to the first hub reachable through the input pins.</summary>
            /// <remarks>The template doesn't have any features. It's only the fastest way to create such jump.</remarks>
            BackJump

        }

        /// <inheritdoc cref="Templates"/>
        public enum Flow
        {
            /// <summary>No template or not a particularly important one.</summary>
            None,
            /// <summary>Main quests.</summary>
            Quest,
            /// <summary>Complex, but optional quests.</summary>
            Sidequest,
            /// <summary>Simple, forgettable quests.</summary>
            Diversion,

            /// <summary>A flow fragment corresponding to a general place in the world.</summary>
            /// <remarks>The whole area of a single game chapter.</remarks>
            Area,
            /// <summary>A flow fragment corresponding to a specific location.</summary>
            /// <remarks>One in-game level or a scene in Unity.</remarks>
            Subarea,
            /// <summary>A flow fragment corresponding to a small but independent part of a <see cref="Subarea"/>.</summary>
            /// <remarks>A house or a cave.</remarks>
            Region,

            /// <summary>A part of the story with Dirk as the playable character.</summary>
            DirkFlow,
            /// <summary>A part of the story with Abigail as the playable character.</summary>
            AbigailFlow,
            /// <summary>A part of the story with Arreon as the playable character.</summary>
            ArreonFlow,
            /// <summary>A part of the story with Lym as the playable character.</summary>
            LymFlow,

            /// <summary>A story chapter. Related locations and characters.</summary>
            Chapter,
            /// <summary>An important choice to make in the story.</summary>
            Choice,
            /// <summary>A part of a conversation inside another dialogue.</summary>
            ConversationGroup,
            /// <summary>A choreographed scene without player interaction.</summary>
            Cutscene,
            /// <summary>An irreversible death of an important character.</summary>
            Death,
            /// <summary>Helpful articy:draft links, queries, annotations.</summary>
            Explanation,
            /// <summary>An important boss fight.</summary>
            Fight,
            /// <summary>A slightly more complex puzzle.</summary>
            Puzzle

        }

        /// <inheritdoc cref="Templates"/>
        public enum Dialogues
        {
            /// <summary>No template or not a particularly important one.</summary>
            None,
            /// <summary>Dirk's dialogue.</summary>
            DirkDialogue,
            /// <summary>Abigail's dialogue.</summary>
            AbigailDialogue,
            /// <summary>Arreon's dialogue.</summary>
            ArreonDialogue,
            /// <summary>Lym's dialogue.</summary>
            LymDialogue

        }

        /// <inheritdoc cref="Templates"/>
        public enum Entities
        {
            /// <summary>No template or not a particularly important one.</summary>
            None,

            AntagonistCharacter,
            EnemyCharacter,
            IncognitoCharacter,
            MainCharacter,
            MerchantCharacter,
            PlayableCharacter,
            SupportingCharacter,

            InteractableObject,

            Currency,
            Item,
            QuestItem,
            Weapon,

            /// <summary> Representation of a Newgrounds medal. </summary>
            Medal,

            DiversionInfo,
            QuestInfo,
            SidequestInfo

        }

        /// <summary> Tracking which dialogue options have been used. </summary>
        public enum SimStatus
        {
            Untouched = 1,
            WasOffered = 2,
            WasDisplayed = 3
        }
    }
}
