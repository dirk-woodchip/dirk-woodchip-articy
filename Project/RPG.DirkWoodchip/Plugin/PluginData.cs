﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Media;
using Articy.Api;
using Newtonsoft.Json;

namespace RPG.DirkWoodchip
{
    public class PluginData : INotifyPropertyChanged
    {

        public const string CharacterFeature = "CharacterInfo";
        public const string CharacterDescription = "CharacterInfo.Description";
        public const string CharacterDialogue = "CharacterInfo.DialogueSlot";
        public const string CharacterLocation = "CharacterInfo.LocationSlot";

        /// <summary> The names of all four main characters in the game. </summary>
        public static readonly string[] MainCharacters =
        {
                nameof(Characters.Dirk), 
                nameof(Characters.Arreon),
                nameof(Characters.Abigail),
                nameof(Characters.Lym)
        };
        /// <summary>Main character names.</summary>
        public enum Characters
        {

            Dirk,
            Arreon,
            Abigail,
            Lym,
            Cyrhus,
            Nemoria

        }

        /// <summary>All global variable groups.</summary>
        public enum GlobalVariables
        {

            NONE = 0,

            Abigail,
            Banter,
            Dirk,
            DirkArreon,
            DirkCity,
            DirkDream,
            DirkForest,
            DirkMorten,
            DirkVolmek,
            DirkWitch,
            DirkWoods,
            Hero,
            Lym,
            Story

        }

        /// <summary>Main character colours.</summary>
        public static readonly Dictionary<string, Color> ColorTable = new Dictionary<string, Color>
        {
                {"Village", Color.FromRgb(0x00, 0xAF, 0x4F)},
                {"Royal", Color.FromRgb(0xC0, 0x4F, 0x4B)},
                {"Other", Color.FromRgb(0x3D, 0x3D, 0x3D)},
                {"Forest", Color.FromRgb(0x76, 0x92, 0x3B)},
                {"Runic", Color.FromRgb(0x4D, 0x80, 0xBD)},
                {"Dragon", Color.FromRgb(0xF7, 0x95, 0x45)},
                {"Blank", Color.FromRgb(0xFF, 0xFF, 0xFF)}
    };

        /// <summary>Connection priority for the Unity's Dialogue System.</summary>
        public enum Priority
        {

            High,
            AboveNormal,
            Normal,
            BelowNormal,
            Low

        }

        /// <summary>Connection priority colours for the Unity's Dialogue System.</summary>
        public static readonly Dictionary<Priority, Color> PriorityColorTable = new Dictionary<Priority, Color>
        {
                {Priority.High, Color.FromRgb(0xFF, 0x00, 0x00)},
                {Priority.AboveNormal, Color.FromRgb(0xFF, 0xC0, 0x00)},
                {Priority.Normal, Color.FromRgb(0xC8, 0xE2, 0xE7)},
                {Priority.BelowNormal, Color.FromRgb(0xFF, 0xFF, 0x00)},
                {Priority.Low, Color.FromRgb(0x92, 0xD0, 0x50)}
        };

        public PluginData(ApiSession aSession)
        {
            Session = aSession;
        }

        [JsonIgnore] public ApiSession Session { get; set; }

        string checkAll = false.ToString();

        public string CheckAll
        {
            get { return checkAll; }
            set
            {
                checkAll = value;
                RaisePropertyChanged("CheckAll");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string aName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(aName));
        }

        public void SaveSettings()
        {
            string settingsPath = Session.GetProjectFolderPath() + "Sessions\\DWPluginSettings.json";
            try
            {
                File.WriteAllText(settingsPath, JsonConvert.SerializeObject(this, Formatting.Indented));
                // TODO: Save settings
            }
            catch (System.Exception)
            {
                // TODO: Log errors
            }
        }

    }
}
