﻿using System.Collections.Generic;
using System;
using System.Linq;
using Articy.Api;
using static RPG.DirkWoodchip.Templates;

namespace RPG.DirkWoodchip
{

    /// <summary>All kinds of helpful methods to navigate and filter objects in articy:draft.</summary>
    public static class PluginUtils
    {

        /// <summary>Sets a predefined template from <see cref="Templates"/> for this object.</summary>
        /// <param name="aObject">The object whose template we want to set.</param>
        /// <param name="template">A template defined in one of the enums in the <see cref="Templates"/> class.</param>
        /// <param name="aSetColor"></param>
        public static void SetTemplate(this ObjectProxy aObject, Enum template, bool aSetColor = true)
        {
            if (!IsValidTemplateName(template.ToString())) return;
            aObject.SetTemplate(template.ToString(), aSetColor);
        }

        /// <summary>Returns the nearest flow element of given type from which the current flow element can be accessed.</summary>
        /// <param name="aObject">The current flow element, root of the search.</param>
        /// <param name="objectType">The type of flow element to look for.</param>
        /// <param name="template">The optional template of the object to look for. Keep empty for any template.</param>
        /// <remarks>Breath-first search through the input flow elements to find the first element of a certain type.</remarks>
        /// <returns>The first discovered input element of the given type or <see langword="null"/> if none is found.</returns>
        public static ObjectProxy GetLatestFlowElement(ObjectProxy aObject, ObjectType objectType, string template = "")
        {
            var flowQueue = new Queue<ObjectProxy>(GetInputFlowElements(aObject));
            var visited = new HashSet<ObjectProxy>();
            while (flowQueue.Count > 0)
            {
                ObjectProxy flow = flowQueue.Dequeue();

                if (visited.Contains(flow)) continue;
                else visited.Add(flow);

                if (flow.ObjectType == objectType)
                {
                    if (string.IsNullOrEmpty(template)) return flow;
                    else if (flow.GetTemplateTechnicalName() == template) return flow;
                }
                else GetInputFlowElements(flow).ToList().ForEach(flowQueue.Enqueue);
            }
            return null;
        }

        /// <summary>Returns all input flow elements of a given object.</summary>
        public static IEnumerable<ObjectProxy> GetInputFlowElements(ObjectProxy aObject)
        {
            var latestObjects = from con in GetInputConnections(aObject)
                                select con[ObjectPropertyNames.Source] as ObjectProxy;
            var flowElements = latestObjects.Where(flow => flow != null); //? Shouldn't ever be null.
            return flowElements;
        }

        /// <summary> Returns the source and target flow objects from a connection. </summary>        
        public static (ObjectProxy source, ObjectProxy target) GetConnectionSourceAndTarget(ObjectProxy aConnection)
        {
            var outputPin = aConnection[ObjectPropertyNames.SourcePin] as ObjectProxy;
            var inputPin = aConnection[ObjectPropertyNames.TargetPin] as ObjectProxy;

            ObjectProxy source = (from flow in aConnection.GetParent().GetChildren()
                                  where flow.ObjectType == ObjectType.DialogueFragment &&
                                        flow.GetOutputPins().Contains(outputPin)
                                  select flow).FirstOrDefault();
            ObjectProxy target = (from flow in aConnection.GetParent().GetChildren()
                                  where flow.ObjectType == ObjectType.DialogueFragment &&
                                        flow.GetInputPins().Contains(inputPin)
                                  select flow).FirstOrDefault();
            return (source, target);
        }

        /// <summary>Returns all input connections of a given flow element.</summary>
        /// <param name="aFlow">The flow element whose input connections we look for.</param>
        /// <remarks>Looks through all objects inside the same flow node to see if their output pins are connected to the object's input pins.</remarks>
        /// <returns>Object's input connection or <see cref="Enumerable.Empty{T}"/> if it's unconnected or a wrong object type.</returns>
        public static IEnumerable<ObjectProxy> GetInputConnections(ObjectProxy aFlow)
        {
            if (!aFlow.IsConnectable) return Enumerable.Empty<ObjectProxy>(); // Not a connectable flow element

            var inputPins = aFlow.GetInputPins();
            var connections = from flow in aFlow.GetParent().GetChildren()
                              where flow.ObjectType == ObjectType.Connection && inputPins.Contains(flow[ObjectPropertyNames.TargetPin] as ObjectProxy)
                              select flow;
            // TODO: This doesn't find connections of a newly created Dialogue Fragment
            return connections;
        }

        /// <summary>Returns all output connections of a given flow element.</summary>
        /// <param name="aFlow">The flow element whose output connections we look for.</param>
        /// <remarks>Looks through all objects inside the same flow node to see if their input pins are connected to the object's output pins.</remarks>
        /// <returns>Object's output connection or <see cref="Enumerable.Empty{T}"/> if it's unconnected or a wrong object type.</returns>
        public static IEnumerable<ObjectProxy> GetOutputConnections(ObjectProxy aFlow)
        {
            if (!aFlow.IsConnectable) return Enumerable.Empty<ObjectProxy>(); // Not a connectable flow element

            var outputPins = aFlow.GetOutputPins();
            var connections = from flow in aFlow.GetParent().GetChildren()
                              where flow.ObjectType == ObjectType.Connection && outputPins.Contains(flow[ObjectPropertyNames.SourcePin] as ObjectProxy)
                              select flow;
            return connections;
        }

        /// <summary>Gets all pins of a flow object which aren't connected to anything.</summary>
        /// <param name="aFlow">Flow object whose pins we want.</param>
        /// <param name="withScripts">UNAVAILABLE: Include pins which are unconnected, but have condition or instruction inside.</param>
        /// <returns>A pair of (input, output) lists of unconnected pins</returns>
        public static Tuple<List<ObjectProxy>, List<ObjectProxy>> GetUnconnectedPins(ObjectProxy aFlow, bool withScripts = true)
        {
            if (!withScripts)
            {
                throw new NotImplementedException("The option to exclude unconnected pins with expressions is not implemented yet.");
            }


            var inputPins = aFlow.GetInputPins();
            var outputPins = aFlow.GetOutputPins();
            var connectedInputPins = GetInputConnections(aFlow).Select(inCon => inCon[ObjectPropertyNames.TargetPin] as ObjectProxy).ToList();
            var connectedOutputPins = GetOutputConnections(aFlow).Select(outCon => outCon[ObjectPropertyNames.SourcePin] as ObjectProxy).ToList();

            var unconnectedInputPins = inputPins.Where(inPin => !connectedInputPins.Contains(inPin));
            var unconnectedOutputPins = outputPins.Where(outPin => !connectedOutputPins.Contains(outPin));

            // TODO: Param 'with scripts'

            return new Tuple<List<ObjectProxy>, List<ObjectProxy>>(unconnectedInputPins.ToList(), unconnectedOutputPins.ToList());
        }

        /// <summary>Checks whether a template is defined in the <see cref="Templates"/> class (and therefore used in the project).</summary>
        /// <param name="template">Template technical name.</param>
        public static bool IsValidTemplateName(string template)
        {
            // TODO: Check whether it correctly accepts/rejects
            return template.InEnum(typeof(DialogueFragments),
                                   typeof(Dialogues),
                                   typeof(Flow),
                                   typeof(Hubs),
                                   typeof(Jumps));
        }

        /// <summary>Return all characters who don't have an assigned dialogue yet.</summary>
        public static List<ObjectProxy> FilterCharacters(IEnumerable<ObjectProxy> aSelectedObjects)
        {
            return aSelectedObjects.Where(ch => ch.ObjectType == ObjectType.Entity &&
                                                ch.HasProperty(PluginData.CharacterDialogue) &&
                                                ch.HasProperty(PluginData.CharacterLocation) &&
                                                ch[PluginData.CharacterDialogue] == null).ToList();
        }

        /// <summary> Returns the speaker of a dialogue fragment. </summary>
        public static ObjectProxy GetSpeaker(ObjectProxy aDialogueFragment)
        {
            if (aDialogueFragment.ObjectType != ObjectType.DialogueFragment) return null;
            return aDialogueFragment[ObjectPropertyNames.Speaker] as ObjectProxy;
        }

        /// <summary> Checks whether a given <see cref="ObjectProxy"/> has a specified template. </summary>
        /// <param name="aObject">An object to check.</param>
        /// <param name="template">A template specified under <see cref="Templates"/>.</param>
        /// <returns>True if the object has the specified template.</returns>
        public static bool HasTemplate(this ObjectProxy aObject, Enum template)
        {
            Type[] allowedTypes = {typeof(DialogueFragments), typeof(Dialogues), typeof(Flow), typeof(Hubs), typeof(Jumps)};
            if (!allowedTypes.Contains(template.GetType()))
            {
                throw new ArgumentException($"The {nameof(template)} parameter must be chosen from: \n{string.Join<Type>(",", allowedTypes)}");
            }
            return aObject.GetTemplateTechnicalName() == template.ToString();
        }

        /// <summary> Checks whether a template is one of provided templates. </summary>
        /// <param name="template"> The actual template of an object. </param>
        /// <param name="allowedTemplates"> The templates to be tested against. </param> 
        /// <returns> True if the template matches one of the specified templates. </returns>
        public static bool AnyOf(this Enum template, params Enum[] allowedTemplates)
        {
            Type[] templateTypes = {typeof(DialogueFragments), typeof(Dialogues), typeof(Flow), typeof(Hubs), typeof(Jumps)};
            if (!templateTypes.Contains(template.GetType()))
            {
                throw new ArgumentException($"The {nameof(template)} parameter must be chosen from: \n{string.Join<Type>(",", templateTypes)}");
            }
            if (!allowedTemplates.All(allowed => templateTypes.Contains(allowed.GetType())))
            {
                throw new ArgumentException($"The {nameof(allowedTemplates)} parameters must be chosen from: \n{string.Join<Type>(",", templateTypes)}");
            }
            return allowedTemplates.Contains(template);
        }

        /// <summary> Runs a SQL SELECT query limited to the first object found. </summary>
        /// <param name="session">The current articy:draft session.</param>
        /// <param name="aObject">The object found.</param>
        /// <param name="from">Where to start the search. E.g. "Entities" or "Flow".</param>
        /// <param name="where">The SQL query where clause. E.g. "WHERE TemplateName == 'Dirk'".</param>
        /// <returns>True if an object was found and successfully returned.</returns>
        public static bool TrySelectOne(this ApiSession session, out ObjectProxy aObject, string from = "Project", string where = "")
        {
            if (!where.ToUpperInvariant().StartsWith("WHERE"))
            {
                throw new ArgumentException("The where clause of your query should start with \"WHERE\".", nameof(where));
            }
            string fullQuery = $"SELECT * FROM {from} {where} LIMIT 1";
            ResultSet queryResults = session.RunQuery(fullQuery);
            if (queryResults.Rows.Count > 0)
            {
                aObject = queryResults.Rows[0];
                return true;
            }
            else
            {
                aObject = null;
                return false;
            }
        }

        /// <summary> Determines whether an object is a descendant of an object with a specified technical name. </summary>
        public static bool IsDescendantOf(this ObjectProxy child, string technicalName)
        {
            ObjectProxy parent = child.GetParent();
            do
            {
                if (parent.GetTechnicalName() == technicalName)
                {
                    return true;
                }
                parent = parent.GetParent();
            } while (parent != null);
            return false;
        }

        /// <summary> Checks whether a number falls within a certain range. </summary>
        public static bool IsWithin(this int value, int minimum, int maximum)
        {
            return value >= minimum && value <= maximum;
        }

        /// <summary> Clamps a number within a specified range. </summary>
        public static int Clamp(this int value, int minimum, int maximum)
        {
            return (value < minimum) ? minimum : (value > maximum) ? maximum : value;
        }

        /// <summary> Capizalizes the very first letter of the provided <see langword="string"/>. </summary>
        /// <param name="value">A non-empty string.</param>
        /// <returns> A string with the first letter uppercased.</returns>
        public static string UppercaseFirst(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException("The string to uppercase can't be empty or null.", nameof(value));
            }
            return char.ToUpper(value[0]) + value.Substring(1); // Capitalize first word
        }

        /// <summary> Determines whether a given string value is defined in one of the enums. </summary>
        /// <param name="value">The string value expected to match one of the enum values.</param>
        /// <param name="enums">The enum types.</param>
        /// <returns>True if the value can be found in one of the enum types.</returns>
        public static bool InEnum(this string value, params Type[] enums)
        {
            if (string.IsNullOrWhiteSpace(value)) return false;
            foreach (Type enumType in enums)
            {
                if (!enumType.IsEnum)
                {
                    throw new ArgumentException("The passed type is not an enum.", nameof(enums));
                }
                try
                {
                    object _ = Enum.Parse(enumType, value);
                }
                catch (ArgumentException)
                {
                    // not found
                    continue;
                }
                return true; // found in the current enumType
            }
            return false;
        }
        /// <summary> Creates a <see cref="HashSet&lt;T&gt;"/> from an <see cref="IEnumerable&lt;outT&gt;"/>. </summary>
        /// <typeparam name="TSource"> The type of the elements of <paramref name="source"/>. </typeparam>
        /// <param name="source"> The <see cref="IEnumerable&lt;outT&gt;"/> to create <see cref="HashSet&lt;T&gt;"/> from.</param>
        /// <param name="comparer"> The <see cref="IEqualityComparer&lt;inT&gt;"/> implementation to use when comparing values in the set,
        /// or <see langword="null"/> to use the default <see cref="EqualityComparer&lt;T&gt;"/> implementation for the set type. </param>
        /// <exception cref="ArgumentException"><paramref name="source"/> is null. </exception>
        /// <returns> A <see cref="HashSet&lt;T&gt;"/> that contains elements from the input sequence. </returns>
        public static HashSet<TSource> ToHashSet<TSource>(this IEnumerable<TSource> source, IEqualityComparer<TSource> comparer = null)
        {
            return new HashSet<TSource>(source, comparer);
        }

        /// <summary> Return true when an enumerable does not contain any provided values. </summary>
        public static bool NotContains<T>(this IEnumerable<T> enumerable, params T[] values)
        {
            return values.All(value => !enumerable.Contains(value));
        }

    }

}
