﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Articy.Api;
using Articy.Api.Plugins;
using Newtonsoft.Json;
using RPG.DirkWoodchip.Trello;
using Log = Articy.Utils.LogEntryType;
using Texts = LIds.RPG.DirkWoodchip.Plugin;
using Names = Articy.Api.ObjectPropertyNames;
using static RPG.DirkWoodchip.Templates;

namespace RPG.DirkWoodchip
{
    /// <inheritdoc />
    /// <summary>
    ///     Public implementation part of plugin code, contains all overrides of the plugin class.
    /// </summary>
    public class Plugin : MacroPlugin
    {
        const string MeetVariablePlaceholder = "// DirkArea.CharacterMet";

        // static readonly Regex upperCaseLetters = new Regex(@"((?<=\p{Ll})\p{Lu}|\p{Lu}(?=\p{Ll}))");
        const string BlankDialogue = "BlankDialogue";
        const string ColorTag = "Color";

        const double GridSize = 25;

        Dictionary<string, Brush> colorBoxCache;

        /// <inheritdoc />
        public override string DisplayName
        {
            get { return LocalizeStringNoFormat(Texts.DisplayName); }
        }

        /// <inheritdoc />
        public override string ContextName
        {
            get { return LocalizeStringNoFormat(Texts.ContextName); }
        }

        /// <summary>Called when an object was created and just before entering the "auto-rename" on new objects.</summary>
        /// <param name="aObject">The recently created object.</param>
        /// <remarks>https://www.nevigo.com/ad3mdk/html/M_Articy_Api_Plugins_MacroPlugin_ObjectCreated.htm</remarks>
        public override bool ObjectCreated(ObjectProxy aObject)
        {
            if (aObject.IsReadOnly) return base.ObjectCreated(aObject);
            switch (aObject.ObjectType)
            {
                case ObjectType.DialogueFragment:
                    AutoassignDialogueFragmentTemplate(aObject);
                    break;
            }
            return base.ObjectCreated(aObject);
        }

        /// <inheritdoc />
        /// <summary>Called after an object gets a new template.</summary>
        /// <param name="aObject">The object whose template was changed.</param>
        /// <remarks>https://www.nevigo.com/ad3mdk/html/M_Articy_Api_Plugins_MacroPlugin_TemplateChanged.htm</remarks>
        public override bool TemplateChanged(ObjectProxy aObject)
        {
            if (aObject.IsReadOnly) return base.TemplateChanged(aObject);
            // Changing templates
            string templateName = aObject.GetTemplateTechnicalName();

            switch (aObject.ObjectType)
            {
                case ObjectType.DialogueFragment when Enum.TryParse(templateName, out DialogueFragments dialogueFragmentTemplate):
                    AutoassignSpeakers(aObject, dialogueFragmentTemplate);
                    if (dialogueFragmentTemplate == DialogueFragments.ChoiceDialogueFragment) SetupChoiceFragment(aObject);
                    break;
                case ObjectType.Hub when Enum.TryParse(templateName, out Hubs hubTemplate):
                    AutoassignHubNames(aObject, hubTemplate);
                    // if (hubTemplate == Hubs.ChoiceHub) SetupChoiceHub(aObject); 
                    // TODO: ↑ Set 'WasOffered' SimStatus for all descendants in instruction | When connecting 'ChoiceFg' to 'ChoiceHb'
                    break;
                case ObjectType.Jump when Enum.TryParse(templateName, out Jumps jumpTemplate):
                    AutoassignJump(aObject, jumpTemplate);
                    break;
            }

            return base.TemplateChanged(aObject);
        }        

        /// <summary>Clears all unconnected pins of a flow object.</summary>
        /// <param name="aFlow">Flow object with unconnected pins.</param>
        void ClearUnconnectedPins(ObjectProxy aFlow)
        {
            Tuple<List<ObjectProxy>, List<ObjectProxy>> unconnected = PluginUtils.GetUnconnectedPins(aFlow, false);
            try
            {
                foreach (ObjectProxy inPin in unconnected.Item1)
                    if (CanRemovePin(aFlow, inPin, true))
                        aFlow.RemoveInputPin((int) inPin[Names.PinIndex]);
                foreach (ObjectProxy outPin in unconnected.Item2)
                    if (CanRemovePin(aFlow, outPin, false))
                        aFlow.RemoveOutputPin((int) outPin[Names.PinIndex]);
            }
            catch (ArticyApiException ex)
            {
                AddLogEntry($"Error ({(ErrorCode) ex.ErrorCode}) when trying to remove unconnected pins:\n{ex.Message}\n", Log.Error);
            }
        }

        /// <summary>Sets the default NPC template for any newly created dialogue fragment without one.</summary>
        /// <param name="aDialogueFragment"></param>
        /// <param name="template">The default template to assign.</param>
        void AutoassignDialogueFragmentTemplate(ObjectProxy aDialogueFragment, DialogueFragments template = DialogueFragments.CharacterDialogueFragment)
        {
            //! Dialogue Fragment with no template
            if (string.IsNullOrEmpty(aDialogueFragment.GetTemplateTechnicalName()))
                try
                {
                    aDialogueFragment.SetTemplate(template);
                }
                catch (ArticyApiException ex)
                {
                    AddLogEntry($"Error ({(ErrorCode) ex.ErrorCode}) while setting a Dialogue Fragment template with {template.ToString()}.\n{ex.Message}\n", Log.Error);
                    aDialogueFragment.SetTemplate(null);
                }
        }

        /// <summary>Automatically assigns the speaker in main characters' dialogue fragments.</summary>
        /// <param name="aDialogueFragment">Dialogue fragment to assign speaker to.</param>
        /// <param name="template"></param>
        void AutoassignSpeakers(ObjectProxy aDialogueFragment, DialogueFragments template)
        {
            string entityName = default;
            switch (template)
            {
                case DialogueFragments.DirkDialogueFragment:
                    entityName = PluginData.Characters.Dirk.ToString();
                    break;
                case DialogueFragments.ArreonDialogueFragment:
                    entityName = PluginData.Characters.Arreon.ToString();
                    break;
                case DialogueFragments.AbigailDialogueFragment:
                    entityName = PluginData.Characters.Abigail.ToString();
                    break;
                case DialogueFragments.LymDialogueFragment:
                    entityName = PluginData.Characters.Lym.ToString();
                    break;            
            }
            if (!string.IsNullOrEmpty(entityName))
            {
                string speakerQuery = $"SELECT * FROM Entities WHERE DisplayName == '{entityName.Replace("'", "''")}' LIMIT 1";
                ResultSet queryResults = Session.RunQuery(speakerQuery);
                if (queryResults.Rows.Count > 0)
                {
                    ObjectProxy speaker = queryResults.Rows[0];
                    aDialogueFragment[Names.Speaker] = speaker;
                }
            }
        }

        /// <summary>Automatically assigns the <see cref="ObjectType.Hub" /> name for important and unique hubs.</summary>
        /// <param name="aHub">A hub whose template has just been changed.</param>
        /// <param name="template">Template of the hub.</param>
        static void AutoassignHubNames(ObjectProxy aHub, Hubs template)
        {
            switch (template)
            {
                default:
                    // Any other hub type
                    return;
                case Hubs.RootHub:
                    aHub.SetDisplayName("Root");
                    break;
                case Hubs.EndHub:
                    aHub.SetDisplayName("End");
                    break;
                case Hubs.MeetHub:
                    aHub.SetDisplayName("Meet");
                    break;
            }
        }
        
        private void SetupChoiceFragment(ObjectProxy aDialogueFragment)
        {
            var choiceHub = PluginUtils.GetLatestFlowElement(aDialogueFragment, ObjectType.Hub, nameof(Hubs.ChoiceHub));
            if (choiceHub == null)
            {
                // TODO: Insert a hub in front this and all siblings instead
                aDialogueFragment.SetTemplate(string.Empty);
                return;
            }
            const string selectedFragment = "Selected.Fragment";
            const string simStatus = "Visited.SimStatus";

            string CompareSim(SimStatus sim) => $"getProp(self, \"{simStatus}\") == {(int)sim}";

            //! Set condition
            var condition = aDialogueFragment.GetInputPin(0);
            // string alreadyChosen = $"getProp(getObj(\"0x0{choiceHub.Id:X}\"), \"{selectedFragment}\") == self";
            string yetToChoose = $"getProp(getProp(getObj(\"0x0{choiceHub.Id:X}\"), \"{selectedFragment}\"), \"Id\") == 0";
            string alreadyChosen = CompareSim(SimStatus.WasDisplayed);
            // string yetToChoose = CompareSim(SimStatus.Untouched);
            condition[Names.Expression] = $"{alreadyChosen} || {yetToChoose}";
            //! Set instructions
            var instruction = aDialogueFragment.GetOutputPin(0);
            string lockThis = $"setProp(getObj(\"0x0{choiceHub.Id:X}\"), \"{selectedFragment}\", self)";
            string setSim = $"setProp(self, \"{simStatus}\", {(int) SimStatus.WasDisplayed})";
            instruction[Names.Expression] = $"{lockThis};\n{setSim}";
        }
        /// <summary>
        ///     Based on the template of a <see cref="ObjectType.Jump" />, it backtracks through the flow and assigns the
        ///     first corresponding <see cref="ObjectType.Hub" /> to it.
        /// </summary>
        /// <param name="aJump">A jump whose template has just been set.</param>
        /// <param name="jumpTemplate">Template of the jump.</param>
        void AutoassignJump(ObjectProxy aJump, Jumps jumpTemplate)
        {
            //! Assigns a hub template base on the jump template
            Hubs? hubTemplate = null;
            switch (jumpTemplate)
            {
                case Jumps.RootJump:
                    hubTemplate = Hubs.RootHub;
                    break;
                case Jumps.EndJump:
                    hubTemplate = Hubs.EndHub;
                    break;
                case Jumps.BackJump:
                    AssignNearestHub(aJump);
                    break;
            }
            switch (hubTemplate)
            {
                case Hubs.RootHub:
                case Hubs.EndHub:
                    string hubQuery = $"SELECT * FROM SELF:Parent WHERE ObjectType == Hub and TemplateName == '{hubTemplate.ToString()}' LIMIT 1";
                    ResultSet queryResults = Session.RunQuery(hubQuery, aJump);
                    if (queryResults.Rows.Count > 0)
                    {
                        ObjectProxy targetHub = queryResults.Rows[0];
                        aJump[Names.Target] = targetHub;
                    }
                    break;
            }
        }

        /// <summary>Assigns the nearest hub element to the jump.</summary>
        /// <param name="aJump">A jump to start searching from.</param>
        static void AssignNearestHub(ObjectProxy aJump)
        {
            ObjectProxy targetHub = PluginUtils.GetLatestFlowElement(aJump, ObjectType.Hub);
            if (targetHub == null) aJump.SetTemplate(null);
            else if (targetHub.GetTemplateTechnicalName() == nameof(Hubs.RootHub)) aJump.SetTemplate(Jumps.RootJump);
            else if (targetHub.GetTemplateTechnicalName() == nameof(Hubs.EndHub)) aJump.SetTemplate(Jumps.EndJump);
            else aJump[Names.Target] = targetHub; //! Any regular hub
        }


        /// <summary>
        ///     Requests command descriptors to create a context menu. Also provides information about the source context of
        ///     the request.
        /// </summary>
        /// <param name="aSelectedObjects">
        ///     The list of currently selected objects. The list is null if the command was called from
        ///     the ribbon toolbars "global commands" button.
        /// </param>
        /// <param name="aContext">The context from where the menu entries are requested.</param>
        /// <returns>The list of commands the plugin wants to add to the context menu.</returns>
        public override List<MacroCommandDescriptor> GetMenuEntries(List<ObjectProxy> aSelectedObjects, ContextMenuContext aContext)
        {
            var result = new List<MacroCommandDescriptor>();
            switch (aContext)
            {
                case ContextMenuContext.Global:
                    // Entries for the "global" commands of the ribbon menu are requested
                    var openWindow = new MacroCommandDescriptor
                    {
                            RecordUndoRedo = false,
                            ModifiesData = false,
                            CaptionLid = "Settings", // {p:$self}
                            Execute = OpenToolkitWindow
                    };
                    result.Add(openWindow);
                    return result;
                default:
                    // Normal context menu when working in the content area, navigator, search   
                    List<ObjectProxy> flows = aSelectedObjects.Where(o => o.IsConnectable).ToList();
                    if (flows.Count > 0)
                    {
                        List<ObjectProxy> dFrags = flows.Where(flow => flow.ObjectType == ObjectType.DialogueFragment).ToList();
                        List<ObjectProxy> charDFrags = dFrags.Where(flow => flow.HasTemplate(DialogueFragments.CharacterDialogueFragment)).ToList();
                        if (charDFrags.Count > 0)
                        {
                            var passSpeaker = new MacroCommandDescriptor
                            {
                                    RecordUndoRedo = true,
                                    ModifiesData = true,
                                    CaptionLid = "Pass speaker from previous",
                                    Execute = PassSpeaker,
                                    UserData = charDFrags
                            };
                            result.Add(passSpeaker);
                        }

                        if (dFrags.Count == 1)
                        {
                            var addMoreFragments = new MacroCommandDescriptor
                            {
                                    RecordUndoRedo = true,
                                    ModifiesData = true,
                                    CaptionLid = "Add 3 more fragments",
                                    Execute = AddFragments,
                                    UserData = dFrags.Single()
                            };
                            result.Add(addMoreFragments);
                        }

                        List<ObjectProxy> jumps = flows.Where(flow => flow.ObjectType == ObjectType.Jump).ToList();
                        if (jumps.Any())
                        {
                            var backJump = new MacroCommandDescriptor
                            {
                                    RecordUndoRedo = true,
                                    ModifiesData = true,
                                    CaptionLid = "Backtrack to the neareast hub",
                                    Execute = BackJumps,
                                    UserData = jumps
                            };
                            result.Add(backJump);
                        }

                        IEnumerable<ObjectProxy> multiplePinFlows = flows.Where(flow => flow.ObjectType == ObjectType.FlowFragment || flow.ObjectType == ObjectType.Dialogue);
                        if (multiplePinFlows.Any())
                        {
                            var removeUnconnectedPins = new MacroCommandDescriptor
                            {
                                    RecordUndoRedo = true,
                                    ModifiesData = true,
                                    CaptionLid = "Remove unconnected pins", // {p:$self}
                                    Execute = RemoveUnconnectedPins,
                                    UserData = multiplePinFlows
                            };
                            result.Add(removeUnconnectedPins);
                        }
                    }
                    List<ObjectProxy> colorable = aSelectedObjects.Where(o => o.HasColor && !o.IsReadOnly).ToList();
                    if (colorable.Any())
                        foreach (string colorKey in PluginData.ColorTable.Keys)
                        {
                            var setColor = new MacroCommandDescriptor
                            {
                                    RecordUndoRedo = true,
                                    ModifiesData = true,
                                    CaptionLid = $"Change color\\{{p:{colorKey}{ColorTag}}}{colorKey}", // {p:$self}
                                    Execute = SetColor,
                                    UserData = PluginData.ColorTable[colorKey]
                            };
                            result.Add(setColor);
                        }
                    List<ObjectProxy> characters = PluginUtils.FilterCharacters(aSelectedObjects);
                    if (characters.Any())
                    {
                        string caption = characters.Count > 1 ? $"all {characters.Count} characters" : "the character";
                        var setupNewDialogue = new MacroCommandDescriptor
                        {
                                CaptionLid = $"Create a Dialogue for {caption}", // {p:$self}
                                ModifiesData = true,
                                Execute = NewDialogueFromEntity,
                                UserData = characters
                        };
                        result.Add(setupNewDialogue);
                    }

                    List<ObjectProxy> unassignedQuestFlows = aSelectedObjects
                                                            .Where(flow => flow.ObjectType == ObjectType.FlowFragment
                                                                        && flow.HasProperty("QuestFlow.QuestStrip")
                                                                        && ((IReadOnlyCollection<ObjectProxy>) flow["QuestFlow.QuestStrip"]).Count == 0)
                                                            .ToList();
                    if (unassignedQuestFlows.Any())
                    {
                        string caption = unassignedQuestFlows.Count > 1 ? $"all {unassignedQuestFlows.Count} quests" : "the quest";
                        var setupQuestEntity = new MacroCommandDescriptor
                        {
                                CaptionLid = $"Create a quest entity for {caption}", // {p:$self}
                                ModifiesData = true,
                                Execute = NewQuestEntityFromFlow,
                                UserData = unassignedQuestFlows
                        };
                        result.Add(setupQuestEntity);
                    }

                    List<ObjectProxy> documentsUnderTrello = aSelectedObjects
                                                            .Where(document => document.ObjectType == ObjectType.Document
                                                                            && document.IsDescendantOf(CommentSaver.TrelloRootId))
                                                            .ToList();
                    if (documentsUnderTrello.Any())
                    {
                        string caption = documentsUnderTrello.Count > 1 ? $"all {documentsUnderTrello.Count} cards" : "the card";
                        int limit = 75; // TODO: 5, 25, 100, 250, 1000 | Plugin settings
                        var trelloCommentUpdateSmall = new MacroCommandDescriptor
                        {
                                CaptionLid = $"Trello\\Pull {limit} dialogues for {caption}", // {p:$self}
                                ModifiesData = true,
                                Execute = AddFromTrelloCards,
                                UserData = (limit, documentsUnderTrello, CommentLoader.CommentFilter.Everything)
                        };
                        result.Add(trelloCommentUpdateSmall);
                        var trelloCommentUpdateUnprocessed = new MacroCommandDescriptor
                        {
                                CaptionLid = $"Trello\\Pull unprocessed dialogues for {caption}", // {p:$self}
                                ModifiesData = true,
                                Execute = AddFromTrelloCards,
                                UserData = (CommentLoader.ManateeLimit, documentsUnderTrello, CommentLoader.CommentFilter.Unprocessed)
                        };
                        result.Add(trelloCommentUpdateUnprocessed);
                        var trelloCommentUpdateNonRejected = new MacroCommandDescriptor
                        {
                                CaptionLid = $"Trello\\Pull non-rejected dialogues for {caption}", // {p:$self}
                                ModifiesData = true,
                                Execute = AddFromTrelloCards,
                                UserData = (CommentLoader.ManateeLimit, documentsUnderTrello, CommentLoader.CommentFilter.Unprocessed | CommentLoader.CommentFilter.Accepted)
                        };
                        result.Add(trelloCommentUpdateNonRejected);
                        var trelloCommentUpdate = new MacroCommandDescriptor
                        {
                                CaptionLid = $"Trello\\Pull all dialogues for {caption}", // {p:$self}
                                ModifiesData = true,
                                Execute = AddFromTrelloCards,
                                UserData = (CommentLoader.TrelloApiLimit, documentsUnderTrello, CommentLoader.CommentFilter.Everything)
                        };
                        result.Add(trelloCommentUpdate);
                        var trelloDialogueFlush = new MacroCommandDescriptor
                        {
                                CaptionLid = $"Trello\\Flush dialogues for {caption}", // {p:$self}
                                ModifiesData = true,
                                Execute = FlushDocumentDialogues,
                                UserData = documentsUnderTrello
                        };
                        result.Add(trelloDialogueFlush);
                    }

                    List<ObjectProxy> dialoguesInTrello = aSelectedObjects
                                                         .Where(dialogue => dialogue.ObjectType == ObjectType.Dialogue
                                                                         && dialogue.IsDescendantOf(CommentSaver.TrelloRootId)
                                                                         && dialogue.IsInDocumentContext())
                                                         .ToList();
                    if (dialoguesInTrello.Any())
                    {
                        string caption = dialoguesInTrello.Count > 1 ? $"all {dialoguesInTrello.Count} dialogues" : "dialogue";
                        foreach (bool? rating in new bool?[] {true, false, null})
                        {
                            string ratingStr = rating.HasValue ? (rating.Value ? "Mark used" : "Mark rejected") : "Clear rating";
                            var trelloCommentRate = new MacroCommandDescriptor
                            {
                                    CaptionLid = $"Rate {caption}\\{ratingStr}",
                                    ModifiesData = true,
                                    Execute = MarkTrelloDialogue,
                                    UserData = (rating, dialoguesInTrello)
                            };
                            result.Add(trelloCommentRate);
                        }
                    }

                    return result;
            }
        }

        /// <summary> Adds dialogues to the current document. </summary>
        void AddFromTrelloCards(MacroCommandDescriptor aDescriptor, List<ObjectProxy> aSelectedObjects)
        {
            (int limit, List<ObjectProxy> trelloDocuments, CommentLoader.CommentFilter filter) =
                    (ValueTuple<int, List<ObjectProxy>, CommentLoader.CommentFilter>) aDescriptor.UserData;

            var trello = new CommentSaver(Session, limit);
            foreach (ObjectProxy document in trelloDocuments)
            {
                trello.UpdateTrelloDocument(document, filter);
            }
        }

        void MarkTrelloDialogue(MacroCommandDescriptor aDescriptor, List<ObjectProxy> aSelectedObjects)
        {
            (bool? rating, List<ObjectProxy> trelloDialogues) = (ValueTuple<bool?, List<ObjectProxy>>) aDescriptor.UserData;
            var trello = new CommentSaver(Session);
            foreach (ObjectProxy dialogue in trelloDialogues)
            {
                trello.RateTrelloDialogue(dialogue, rating);
            }
        }
        
        /// <summary> Deletes all entries within a document. </summary>
        void FlushDocumentDialogues(MacroCommandDescriptor aDescriptor, List<ObjectProxy> aSelectedObjects)
        {
            var trelloDocuments = (List<ObjectProxy>) aDescriptor.UserData;
            var toDelete = new List<ObjectProxy>();
            foreach (ObjectProxy document in trelloDocuments)
            {
                toDelete.AddRange(document.GetChildren());
            }
            foreach (ObjectProxy dialogue in toDelete)
            {
                Session.DeleteObject(dialogue);
            }
        }

        /// <summary> Adds three more dialogue fragments of the same speaker in a line. </summary>
        void AddFragments(MacroCommandDescriptor aDescriptor, List<ObjectProxy> aSelectedObjects)
        {
            var aDialogueFragment = (ObjectProxy) aDescriptor.UserData;
            double positionOffset = GridSize;

            ObjectProxy previousFlow = PluginUtils.GetInputFlowElements(aDialogueFragment).FirstOrDefault();
            if (previousFlow != null) positionOffset = Math.Abs(previousFlow.GetFlowPosition().X - aDialogueFragment.GetFlowPosition().X);

            ObjectProxy parentFlow = aDialogueFragment.GetParent();
            string template = aDialogueFragment.GetTemplateTechnicalName();
            Point position = aDialogueFragment.GetFlowPosition();
            ObjectProxy oldFragment = aDialogueFragment;
            for (int i = 0; i < 3; i++)
            {
                ObjectProxy newFragment = Session.CreateDialogueFragment(parentFlow, string.Empty, template);
                position.X += positionOffset;
                newFragment.SetFlowPosition(position);
                Session.ConnectPins(oldFragment.GetOutputPin(0), newFragment.GetInputPin(0));

                newFragment[Names.Speaker] = oldFragment[Names.Speaker];

                newFragment.SetFlowSize(oldFragment.GetFlowSize());

                oldFragment = newFragment;
            }
        }

        /// <summary>Assigns the nearest hub for each respective jump.</summary>
        void BackJumps(MacroCommandDescriptor aDescriptor, List<ObjectProxy> aSelectedObjects)
        {
            var jumps = (List<ObjectProxy>) aDescriptor.UserData;
            foreach (ObjectProxy jump in jumps) AutoassignJump(jump, Jumps.BackJump);
        }

        /// <summary>Removes all pins which aren't connected to anything else.</summary>
        void RemoveUnconnectedPins(MacroCommandDescriptor aDescriptor, List<ObjectProxy> aSelectedObjects)
        {
            foreach (ObjectProxy flow in (List<ObjectProxy>) aDescriptor.UserData) ClearUnconnectedPins(flow);
        }

        /// <summary> Passes a NPC character from previous dialogue fragments to the selected ones. </summary>
        static void PassSpeaker(MacroCommandDescriptor aDescriptor, List<ObjectProxy> aSelectedObjects)
        {
            // TODO: Won't work properly in case of multiple nodes leading to the same selected target (DFS instead of BFS)

            bool FoundPreviousSpeaker(ObjectProxy aObject)
            {
                return aObject.HasTemplate(DialogueFragments.CharacterDialogueFragment) &&
                       aObject[Names.Speaker] != null;
            }

            var characterFragments = (List<ObjectProxy>) aDescriptor.UserData;
            foreach (ObjectProxy characterFragment in characterFragments)
            {
                ObjectProxy previous = characterFragment;
                do
                {
                    previous = PluginUtils.GetLatestFlowElement(previous, ObjectType.DialogueFragment);
                } while (previous != null && !FoundPreviousSpeaker(previous));
                if (previous == null) continue;
                characterFragment[Names.Speaker] = previous[Names.Speaker];
            }
        }

        /// <summary>Sets the colour of selected objects.</summary>
        static void SetColor(MacroCommandDescriptor aDescriptor, List<ObjectProxy> aSelectedObjects)
        {
            var color = (Color) aDescriptor.UserData;
            foreach (ObjectProxy obj in aSelectedObjects)
                if (obj.HasColor && !obj.IsReadOnly)
                    obj.SetColor(color);
        }

        /// <summary>Creates a prefilled dialogue for a selected characters.</summary>
        void NewDialogueFromEntity(MacroCommandDescriptor aDescriptor, List<ObjectProxy> aSelectedObjects)
        {
            var characters = (List<ObjectProxy>) aDescriptor.UserData;
            foreach (ObjectProxy character in characters)
            {
                string charname = character.GetDisplayName();
                if (character[PluginData.CharacterLocation] == null)
                {
                    ShowMessageBox($"{charname} doesn't have a location assigned.\n", "Missing location", aImage: MessageBoxImage.Warning);
                    continue;
                }
                var location = character[PluginData.CharacterLocation] as ObjectProxy;
                string flowAreaQuery = $"SELECT * FROM Flow WHERE ObjectType == FlowFragment and DisplayName == '{location?.GetDisplayName().Replace("'", "''")}' LIMIT 1";
                ResultSet queryResults = Session.RunQuery(flowAreaQuery);
                if (queryResults.Rows.Count > 0)
                {
                    ObjectProxy flowArea = queryResults.Rows[0];
                    ObjectProxy newDialogue = Session.CreateDialogue(flowArea, charname);
                    character[PluginData.CharacterDialogue] = newDialogue;
                    newDialogue[Names.Text] = character[PluginData.CharacterDescription];
                    SetupNewDialogue(newDialogue, character, location);
                }
                else
                {
                    ShowMessageBox($"No flow fragment {location?.GetDisplayName()} found.", "Invalid area flow fragment", aImage: MessageBoxImage.Warning);
                }
            }
        }

        /// <summary>Automatically assigns the quest related variables when they're not already assigned.</summary>
        /// <remarks>Accessed whenever a flow fragment has a feature called 'QuestInfo'. Creates an entity automatically.</remarks>
        void NewQuestEntityFromFlow(MacroCommandDescriptor aDescriptor, List<ObjectProxy> aSelectedObjects)
        {
            const string questInfo = "QuestInfo";

            const string associatedNode = "QuestFlow";
            const string startingLocation = "Location";
            const string questType = "QuestType";
            const string questReceiver = "PlayerSlot";
            const string chapter = "Chapter";

            const string entitiesQuestFolder = "QuestFolder";
            ObjectProxy questFolder = Session.GetObjectByTechName(entitiesQuestFolder);

            string GetEntityQuestTemplateName(Flow questTemplate)
            {
                var result = Entities.None;
                switch (questTemplate)
                {
                    case Flow.Quest:
                        result = Entities.QuestInfo;
                        break;
                    case Flow.Sidequest:
                        result = Entities.SidequestInfo;
                        break;
                    case Flow.Diversion:
                        result = Entities.DiversionInfo;
                        break;
                }
                return (result != Entities.None) ? result.ToString() : null;
            }

            foreach (ObjectProxy aFlow in (List<ObjectProxy>) aDescriptor.UserData)
            {
                if (!Enum.TryParse(aFlow.GetTemplateTechnicalName(), out Flow flowTemplate)) continue;

                //! Create an entity with a name and a template
                string questTemplateName = GetEntityQuestTemplateName(flowTemplate);
                string questName = string.IsNullOrEmpty(aFlow.GetDisplayName()) ? $"New {flowTemplate}" : aFlow.GetDisplayName();

                ObjectProxy aEntity = Session.CreateEntity(questFolder, questName, questTemplateName);

                //! Assign fields
                aEntity[$"{questInfo}.{associatedNode}"] = aFlow;

                if (Session.TrySelectOne(out ObjectProxy area, "Flow", $"WHERE Template(Equal, {Flow.Subarea}) AND IsAncestorOf(${aFlow.GetTechnicalName()})") ||
                    Session.TrySelectOne(out area, "Flow", $"WHERE Template(Equal, {Flow.Area}) AND IsAncestorOf(${aFlow.GetTechnicalName()})"))
                {
                    aEntity[$"{questInfo}.{startingLocation}"] = area["PointsOfInterest.LocationSlot"];
                }

                if (((int) flowTemplate).IsWithin(1, 3))
                {
                    aEntity[$"{questInfo}.{questType}"] = (int) flowTemplate;
                }

                if (Session.TrySelectOne(out ObjectProxy dirkEntity, "Entities", "WHERE TechnicalName = 'Dirk'"))
                {
                    aEntity[$"{questInfo}.{questReceiver}"] = dirkEntity;
                }

                if (Session.TrySelectOne(out ObjectProxy chapterFlow, "Flow", $"WHERE Template(Equal, {Flow.Chapter}) AND IsAncestorOf(${aFlow.GetTechnicalName()})"))
                {
                    aEntity[$"{questInfo}.{chapter}"] = (int) chapterFlow["Date.Chapter"];
                }

                if (string.IsNullOrWhiteSpace((string) aEntity["Text"]))
                {
                    aEntity["Text"] = aFlow["Text"];        // Set 'Description'
                    aEntity["Description"] = aFlow["Text"]; // Set 'Basic description'
                }
            }
        }

        /// <summary>Creates a <see langword="bool" /> variable to check whether a character has talked to the player ever before.</summary>
        /// <param name="character">The character referred to in the variable.</param>
        /// <param name="location">The location the character appears in.</param>
        /// <returns>A variable formated as <code>PlayerArea.CharacterMet</code>.</returns>
        string CreateMeetVariable(ObjectProxy character = null, ObjectProxy location = null)
        {
            string meetVariable = MeetVariablePlaceholder;
            string locationName = "Area";
            string charname = "Character";
            if (location != null) locationName = location.GetDisplayName().Split(new[] {": "}, StringSplitOptions.RemoveEmptyEntries)[0]; //! [Morten]: [Lakeside]
            if (character != null)
            {
                string[] nameChunks = character.GetDisplayName().Split(' ');
                charname = nameChunks.Aggregate((current, chunk) => current + (char.ToUpper(chunk[0]) + chunk.Substring(1)));
            }

            try
            {
                meetVariable = $"Dirk{locationName}.{charname}Met"; // TODO: Not just Dirk, but all playable characters
                Session.CreateGlobalVariable(meetVariable, false, $"Dirk has met {charname} for the first time in {locationName}.");
            }
            catch (ArticyApiException ex)
            {
                if ((ErrorCode) ex.ErrorCode != ErrorCode.NameNotUnique)
                {
                    AddLogEntry($"Error ({(ErrorCode) ex.ErrorCode}): Global variable error.", Log.Error);
                    ShowMessageBox($"Error while trying to create a global variable {meetVariable}.", $"Invalid global variable ({(ErrorCode) ex.ErrorCode})", aImage: MessageBoxImage.Warning);
                    meetVariable = "// " + meetVariable;
                }
            }
            return meetVariable;
        }

        /// <summary>Creates a rough empty dialogue template.</summary>
        /// <param name="aDialogue">A dialogue to set up.</param>
        /// <param name="aCharacter">The conversant of the dialogue.</param>
        /// <param name="aLocation">The location the dialogue takes place in.</param>
        void SetupNewDialogue(ObjectProxy aDialogue, ObjectProxy aCharacter = null, ObjectProxy aLocation = null)
        {
            ObjectProxy rootHub = Session.CreateHub(aDialogue, "Root", nameof(Hubs.RootHub));
            ObjectProxy endHub = Session.CreateHub(aDialogue, "End", nameof(Hubs.EndHub));
            ObjectProxy meetHub = Session.CreateHub(aDialogue, "Meet", nameof(Hubs.MeetHub));
            ObjectProxy rootJump = Session.CreateJump(aDialogue, "Jump to: [Root]", nameof(Jumps.RootJump));
            ObjectProxy meetConversantFragment = Session.CreateDialogueFragment(aDialogue, "[Greetings.]");
            ObjectProxy greetConversantFragment = Session.CreateDialogueFragment(aDialogue, "[Hello.]");
            ObjectProxy talkConversantFragment = Session.CreateDialogueFragment(aDialogue, "[Hi.]", nameof(DialogueFragments.DirkDialogueFragment));

            try
            {
                Session.ConnectPins(aDialogue.GetInputPin(0), greetConversantFragment.GetInputPin(0));
                Session.ConnectPins(aDialogue.GetInputPin(0), meetHub.GetInputPin(0)).SetColor(PluginData.PriorityColorTable[PluginData.Priority.AboveNormal]);
                Session.ConnectPins(greetConversantFragment.GetOutputPin(0), rootHub.GetInputPin(0));
                Session.ConnectPins(meetHub.GetOutputPin(0), meetConversantFragment.GetInputPin(0));
                Session.ConnectPins(meetConversantFragment.GetOutputPin(0), rootJump.GetInputPin(0));
                Session.ConnectPins(endHub.GetOutputPin(0), aDialogue.GetOutputPin(0));
                Session.ConnectPins(rootHub.GetOutputPin(0), talkConversantFragment.GetInputPin(0));
            }
            catch (ArticyApiException ex)
            {
                AddLogEntry($"Trying to create connection between invalid objects: \n{ex.Message}\n", Log.Error);
            }
            //! Set positions
            greetConversantFragment.SetFlowPosition(-25.0, -125.0);
            meetHub.SetFlowPosition(0.0, -400.0);
            meetConversantFragment.SetFlowPosition(275.0, -475);
            rootHub.SetFlowPosition(300.0, -50.0);
            endHub.SetFlowPosition(575.0, -525.0);
            rootJump.SetFlowPosition(575.0, -450.0);
            talkConversantFragment.SetFlowPosition(575.0, -125.0);
            //! Global variable
            string meetVariable = CreateMeetVariable(aCharacter, aLocation);
            meetHub.GetInputPin(0)[Names.Expression] = $"{meetVariable} == false";
            meetConversantFragment.GetOutputPin(0)[Names.Expression] = $"{meetVariable} = true";
            greetConversantFragment.GetInputPin(0)[Names.Expression] = $"{meetVariable} == true";
            //! Character
            if (aCharacter != null)
            {
                greetConversantFragment[Names.Speaker] = aCharacter;
                meetConversantFragment[Names.Speaker] = aCharacter;
                aDialogue.InsertAttachmentIntoStrip("Attachments", aCharacter, 0);
                if (aCharacter.HasPreviewImage) aDialogue.SetPreviewImage(aCharacter.GetPreviewImage());
            }
            // ClearUnconnectedPins(aDialogue);
            aDialogue.SetTemplate(BlankDialogue);
        }

        /// <summary> Returns a character based on its DisplayName property. </summary>
        ObjectProxy GetCharacter(PluginData.Characters character)
        {
            string query = $"SELECT * FROM Entities WHERE DisplayName = {character.ToString()} LIMIT 1";
            ResultSet queryResults = Session.RunQuery(query);
            return queryResults.Rows.Count > 0 ? queryResults.Rows[0] : null;
        }

        /// <summary>Opens the plugin's preferences window.</summary>
        void OpenToolkitWindow(MacroCommandDescriptor aDescriptor, List<ObjectProxy> aSelectedObjects)
        {
            var window = new ToolkitWindow
            {
                    DataContext = new PluginData(Session)
            };
            bool? result = Session.ShowDialog(window);

            //! True means the user confirmed the dialog, false means he canceled it
            if (result.HasValue && result.Value) SaveSettings();
            else LoadPluginSettings(); // overwrite the "in memory" changes with the previous save on the disk
        }

        /// <inheritdoc cref="MacroPlugin.GetIcon" />
        public override Brush GetIcon(string aIconName)
        {
            switch (aIconName)
            {
                // if you have specified the "IconFile" in the PluginManifest.xml you don't need this case
                // unless you want to have an icon that differs when the plugin is loaded from the non-loaded case
                // or you want to put all icons within the resources of your plugin assembly
                case "$self":
                    // get the main icon for the plugin
                    return Session.CreateBrushFromFile(Manifest.ManifestPath + "Resources\\Icon.png");
                default:
                    if (aIconName.EndsWith(ColorTag))
                    {
                        string colorKey = aIconName.Substring(0, aIconName.LastIndexOf(ColorTag, StringComparison.Ordinal));
                        return CreateColorBrush(colorKey);
                    }
                    break;
            }
            return null;
        }

        Brush CreateColorBrush(string colorKey)
        {
            if (colorBoxCache.TryGetValue(colorKey, out Brush cachedBrush)) return cachedBrush; // Already cached
            var brush = new VisualBrush();
            var rectangle = new Rectangle
            {
                    Width = 16,
                    Height = 16,
                    Fill = new SolidColorBrush(PluginData.ColorTable[colorKey]),
                    Stroke = new SolidColorBrush(Colors.White),
                    StrokeThickness = 1.0
            };
            rectangle.Measure(new Size(rectangle.Width, rectangle.Height));
            rectangle.Arrange(new Rect(new Size(rectangle.Width, rectangle.Height)));

            brush.TileMode = TileMode.None;
            brush.Visual = rectangle;
            colorBoxCache.Add(colorKey, brush);
            return brush;
        }

        #region Settings

        /// <summary>The plugin's settings.</summary>
        public PluginData Settings { get; private set; }

        /// <inheritdoc />
        public override bool ProjectLoaded()
        {
            LoadPluginSettings();
            colorBoxCache = new Dictionary<string, Brush>(StringComparer.InvariantCultureIgnoreCase);
            return base.ProjectLoaded();
        }


        public void LoadPluginSettings()
        {
            string settingsPath = Session.GetProjectFolderPath() + "Sessions\\DWPluginSettings.json";
            try
            {
                // load possible existing settings
                Settings = JsonConvert.DeserializeObject<PluginData>(File.ReadAllText(settingsPath));
                Settings.Session = Session;
            }
            catch (Exception)
            {
                // no settings found, so we create a fresh one
                Settings = new PluginData(Session)
                {
                        CheckAll = "Default Value"
                };
            }
        }

        void SaveSettings()
        {
            string settingsPath = Session.GetProjectFolderPath() + "Sessions\\DWPluginSettings.json";
            try
            {
                File.WriteAllText(settingsPath, JsonConvert.SerializeObject(Settings, Formatting.Indented));
            }
            catch (Exception ex)
            {
                AddLogEntry($"Couldn't save the settings:\n{ex.Message}\n", Log.Warning);
            }
        }

        #endregion
    }
}
