﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RPG.DirkWoodchip {
    /// <summary>
    /// Interakční logika pro ToolkitWindow.xaml
    /// </summary>
    public partial class ToolkitWindow {

        PluginData pluginData;

        public ToolkitWindow() {
            InitializeComponent();
        }

        void WindowLoaded(object sender, RoutedEventArgs e) {
            pluginData = (PluginData) DataContext;
            checkAll.IsChecked = bool.Parse(pluginData.CheckAll);
            // MessageBox.Show($"Parsing: {checkAll.IsChecked}.");
            // TODO: Make it work.
        }

        void CheckBox_Checked(object sender, RoutedEventArgs e) {
            Handle(sender as CheckBox, e);
        }

        void CheckBox_Unchecked(object sender, RoutedEventArgs e) {
            Handle(sender as CheckBox, e);
        }

        void Handle(CheckBox checkBox, RoutedEventArgs e) {
            // Use IsChecked.
            bool flag = checkBox.IsChecked.HasValue && checkBox.IsChecked.Value;

            // Assign Window Title.
            this.Title = $"Check → {flag.ToString()} | Handled: {e.Handled}";
        }

        void SwitchAll(object sender, RoutedEventArgs e) {
            var all = (sender as CheckBox);
            foreach (CheckBox checkBox in WindowExtensions.FindVisualChildren<CheckBox>(this)) {
                checkBox.IsChecked = all?.IsChecked;
            }
            pluginData.CheckAll = all?.IsChecked.ToString();
        }

        void ApplySettings(object sender, RoutedEventArgs e) {
            pluginData.SaveSettings();
            this.Close();
        }
    }

    public static class WindowExtensions {
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject {
            if (depObj != null) {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++) {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child is T visualChild) {
                        yield return visualChild;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child)) {
                        yield return childOfChild;
                    }
                }
            }
        }
    }

}
