﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Manatee.Trello;

namespace RPG.DirkWoodchip.Trello
{
    /// <summary> Creating a connection with Trello and obtaining raw data. </summary>
    public sealed class CommentLoader
    {
        public const string BoardName = "Dirk Woodchip";
        public const int TrelloApiLimit = 1000; // 1000;
        public const int ManateeLimit = 300;
        readonly int defaultLimit;
        ITrelloFactory Factory { get; }

        [Flags]
        public enum CommentFilter : uint
        {
            /// <summary> All comments without any emoji reaction. </summary>
            Unprocessed,
            /// <summary> All comments marked with the <see cref="CommentSaver.AcceptedEmoji"/>. </summary>
            Accepted,
            /// <summary> All comments marked with the <see cref="CommentSaver.RejectedEmoji"/>. </summary>
            Rejected,
            /// <summary> All types of comments. </summary>
            Everything = Unprocessed | Accepted | Rejected
        }

        /// <summary> Creates a new loader to download Trello comments. </summary>
        /// <param name="defaultLimit">The number of comments to download. The maximum *per card* is defined in <see cref="TrelloApiLimit"/>.</param>
        public CommentLoader(int defaultLimit = ManateeLimit)
        {
            this.defaultLimit = defaultLimit;
            TrelloAuthorization.Default.AppKey = "650f9899441140faef6539e7e17610dc";
            TrelloAuthorization.Default.UserToken = "205592193cf28dfae54961ef578146b059fa4865c48ed2479ce9d946006fa679";

            // Comment loading mode            
            Card.DownloadedFields &= ~Card.Fields.Actions;
            Card.DownloadedFields |= Card.Fields.Comments;

            Factory = new TrelloFactory();
        }

        // TODO: ListName/CardName | DateTime range (~ card.Comments.Filter())
        // TODO: Filter by reactions
        public async Task<List<IAction>> GetAllComments(string[] cardPaths, CommentFilter filter = CommentFilter.Everything)
        {
            var comments = new List<IAction>();
            try
            {
                IMe me = await Factory.Me();

                IBoard board = me.Boards.Single(b => b.Name == BoardName);
                await board.Refresh();

                var cards = new List<ICard>(); // TODO: Trello has reported an error with the request: invalid value for plugins
                foreach (string cardPath in cardPaths)
                {
                    (string listName, string cardName) = Dialogue.GetPathSegments(cardPath);
                    IList list = board.Lists.Single(l => l.Name == listName);
                    await list.Refresh();
                    IEnumerable<ICard> newCards = list.Cards.Where(c => c.Name == cardName);

                    cards.AddRange(newCards);
                }

                int commentsToDownload = defaultLimit;
                foreach (ICard card in cards)
                {
                    await card.Refresh();
                    card.Comments.Limit = commentsToDownload.Clamp(0, TrelloApiLimit);
                    await card.Comments.Refresh();
                    // Console.WriteLine($"{card.Name}: {card.Comments.Count()}");
                    foreach (IAction comment in card.Comments)
                    {
                        if (filter != CommentFilter.Everything)
                        {
                            await comment.Reactions.Refresh();
                            Emoji[] emojis = comment.Reactions.Select(r => r.Emoji).ToArray();
                            if ((!(filter.HasFlag(CommentFilter.Accepted) && emojis.Contains(CommentSaver.AcceptedEmoji))) &&
                                (!(filter.HasFlag(CommentFilter.Rejected) && emojis.Contains(CommentSaver.RejectedEmoji))) &&
                                (!(filter.HasFlag(CommentFilter.Unprocessed) && emojis.NotContains(CommentSaver.AcceptedEmoji, CommentSaver.RejectedEmoji))))
                            {
                                continue;
                            }
                        }
                        comments.Add(comment);
                        commentsToDownload--;
                    }
                    if (commentsToDownload <= 0) break; // do not load other cards if the limit is already exhausted
                }
                return comments;
            }
            catch (LicenseException lex)
            {
                Console.WriteLine(lex.Message);
                return comments;
            }
        }

        public async Task<IAction> GetComment(string id)
        {
            IAction comment = Factory.Action(id);
            await comment.Refresh();
            return comment;
        }

        public async Task<List<(string Id, string Text)>> GetRawComments(string[] cardPaths, CommentFilter filter = CommentFilter.Everything)
        {
            List<IAction> comments = await GetAllComments(cardPaths, filter);
            return comments.Select(comment => (comment.Id, comment.Data.Text)).ToList();
        }

        // TODO: Move away from CommentLoader
        public async Task RateComment(string id, Emoji rating)
        {
            IAction comment = await GetComment(id);
            await comment.Reactions.Refresh();
            switch (rating)
            {
                default:
                    await comment.Reactions.Add(rating);
                    break;
                case null:
                    foreach (ICommentReaction reaction in comment.Reactions)
                    {
                        await reaction.Delete();
                    }
                    break;
            }
        }
    }
}
