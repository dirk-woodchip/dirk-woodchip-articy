﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace RPG.DirkWoodchip.Trello
{

    /// <summary> A <see langword="struct"/> representing a single dialogue fragment. </summary>
    public struct DialogueEntry
    {

        const RegexOptions Options = RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.ExplicitCapture | RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled;
        const string EmptyActorFiller = "???";
        const string EmptyLineFiller = "…";
        const int ToStringLimit = 48;

        public string label;
        public int nesting;
        public string actor;
        public string[] stageDirs;
        public string line;

        public string RichTextLine
        {            
            get { return ReplaceMarkdown(line).Replace("\n", "<br/><br/>"); }
        }

        public string NestingMark
        {
            get { return string.Join(" ", Enumerable.Repeat(">>", nesting)); }
        }

        /// <summary> All stage directions joined together and formatted for articy:draft. </summary>
        public string StageDirections
        {
            get
            {
                if (stageDirs.Length == 0) return string.Empty;

                string result = string.Join(" | ", stageDirs.Select(stage => stage.UppercaseFirst()));
                if (!result.EndsWith(".")) result += ".";
                return result;
            }
        }

        /// <summary>Initializes a new instance of the <see cref="DialogueEntry" /> struct.</summary>
        public DialogueEntry(string actor, string line, string label = "", int nesting = 0)
        {
            this.label = label;
            this.nesting = nesting;
            this.actor = actor;
            this.line = line;
            this.stageDirs = ExtractStageDirections(ref this.line);

            if (string.IsNullOrWhiteSpace(this.actor)) this.actor = EmptyActorFiller;
            if (string.IsNullOrWhiteSpace(this.line)) this.line = EmptyLineFiller;
        }

        public DialogueEntry(Match entry, ref int previousNesting)
                : this(actor: entry.Groups["actor"].Value.Trim(),
                       line: entry.Groups["line"].Value.Trim(),
                       label: entry.Groups["label"].Value.Trim(),
                       nesting: GetNesting(entry.Groups["nesting"], ref previousNesting)) { }

        public static int GetNesting(Group nestingGroup, ref int previousNesting)
        {
            // TODO: Doesn't take multiple dialogue options into account
            if (string.IsNullOrEmpty(nestingGroup.Value))
            {
                // Non-nested
                return previousNesting;
            }
            if (string.IsNullOrWhiteSpace(nestingGroup.Value))
            {
                // Nested, but not the first one nested
                return previousNesting = 0;
            }
            // Adjusting nesting levels
            int spaceCount = Regex.Match(nestingGroup.Value, @"(?<spaces>[ ]*)0\.").Groups["spaces"].Value.Length;
            return previousNesting = 1 + spaceCount / 4;
        }

        /// <summary> Returns the dialogue line with rich text tags (for bold and italics) instead of Markdown. </summary>
        /// <param name="text">A dialogue line with bold and italics markdown symbols.</param>
        /// <returns>A dialogue line with rich text.</returns>
        static string ReplaceMarkdown(string text)
        {
            // <strong> must go first:
            text = Regex.Replace(text, @"(\*\*|__) (?=\S) (.+?[*_]*) (?<=\S) \1",
                                 new MatchEvaluator(BoldEvaluator),
                                 RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);

            // Then <em>:
            text = Regex.Replace(text, @"(\*|_) (?=\S) (.+?) (?<=\S) \1",
                                 new MatchEvaluator(ItalicsEvaluator),
                                 RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);

            // Remove <strike>:
            text = Regex.Replace(text, @"(~~) (?=\S) (.+?[*_]*) (?<=\S) \1",
                                 new MatchEvaluator(StrikeThroughEvaluator),
                                 RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);

            // Finally replace hyperlinks:
            text = Regex.Replace(text, @"\[(.+?)\]\((.+?)\)", // TODO: A line fully consisting of a link doesn't get converted
                                 new MatchEvaluator(HyperlinkEvaluator),
                                 RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);

            return text;
        }

        static string ItalicsEvaluator(Match match)
        {
            return $"<i>{match.Groups[2].Value}</i>";
        }

        static string BoldEvaluator(Match match)
        {
            return $"<b>{match.Groups[2].Value}</b>";
        }

        static string StrikeThroughEvaluator(Match match)
        {
            return $"{match.Groups[2].Value}";
        }

        static string HyperlinkEvaluator(Match match)
        {
            return $"<a href=\"{match.Groups[2].Value}\">{match.Groups[1].Value}</a>";
        }

        static string[] ExtractStageDirections(ref string line)
        {
            // **ACTOR**: Text. *[stageDirection]*
            //! Stage directions must be cursive or bold – to make a difference between actions. ([Leave.])
            MatchCollection stageMatches = Regex.Matches(line, @"\*+\[(?<stage>.*?)\]\*+", Options);
            var stageDirections = new List<string>();
            foreach (Match match in stageMatches)
            {
                string stage = match.Groups["stage"].Value;
                stageDirections.Add(stage);

                // Remove extracted stage direction
                var removeRegex = new Regex(Regex.Escape(match.ToString()), Options);
                line = removeRegex.Replace(line, string.Empty, 1);
            }
            line = line.Replace("  ", " ").Trim(); // clear forgotten spaces
            return stageDirections.ToArray();
        }

        /// <summary>Returns the fully qualified type name of this instance.</summary>
        /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
        public override string ToString()
        {
            string Capitalize(string name)
            {
                TextInfo textInfo = new CultureInfo("en-GB", false).TextInfo;
                return textInfo.ToTitleCase(name.ToLower());
            }

            string TrimLine(string speech, int limit)
            {
                return speech.Length <= limit
                        ? speech
                        : $"{speech.Substring(0, limit - 1)}…";
            }

            return $"{Capitalize(this.actor)}: {TrimLine(line, ToStringLimit)}";
        }

    }

}
