﻿using System;
using System.Linq;

namespace RPG.DirkWoodchip.Trello
{

    public struct Dialogue
    {

        public string id;
        public DateTime creationDate;
        public string dialogueOwner; // List/Card
        public DialogueEntry[] dialogueEntries;
        public bool? rating;

        public string CardName
        {
            get { return GetPathSegments(dialogueOwner).CardName; }
        }

        public string ListName
        {
            get { return GetPathSegments(dialogueOwner).ListName; }
        }

        /// <summary>Initializes a new instance of the <see cref="Dialogue"/> struct from its entries
        /// and sets the information about its dialogue owner.</summary>
        public Dialogue(string id, DateTime creationDate, string dialogueOwner, DialogueEntry[] dialogueEntries, bool? rating)
        {
            // TODO: Split into two dialogues when the entry contains another label (remember to change ID)
            this.id = id;
            this.creationDate = creationDate;
            this.dialogueOwner = dialogueOwner;
            this.dialogueEntries = dialogueEntries;
            this.rating = rating;
        }

        /// <summary> Retrieves a specific dialogue entry. </summary>
        /// <param name="index">The zero-based index to retrieve a specific dialogue entry.</param>
        /// <returns>A dialogue entry with the specified index.</returns>
        public DialogueEntry this[int index]
        {
            get { return dialogueEntries[index]; }
        }

        /// <summary>Returns the fully qualified type name of this instance.</summary>
        /// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
        public override string ToString()
        {
            string result = $"Dialogue ({dialogueEntries.Length} lines)";

            if (TryGetFirstLabel(out string firstLabel)) result += $": {firstLabel}";
            return result;
        }

        /// <summary> Tries to retrieve the first label found the dialogue's entries. </summary>
        /// <param name="firstLabel"> The first label found in dialogue entries (usually the first one or none at all).
        /// Ocassionally more than one is present – in which case it's ignored.</param>
        /// <returns>True if a non-empty label was found.</returns>
        public bool TryGetFirstLabel(out string firstLabel)
        {
            firstLabel = dialogueEntries.FirstOrDefault(entry => !string.IsNullOrWhiteSpace(entry.label)).label;
            return !string.IsNullOrWhiteSpace(firstLabel);
        }

        /// <summary> Gets card path segments. </summary>
        /// <param name="cardPath">A path formatted as [List]separator[Card]. A list must not have the <paramref name="separator"/> in its name.</param>
        /// <param name="separator">A symbol separating the list name and the card name. It must not be present in the list name.</param>
        /// <returns>A tuple with a card name and the list it's contained in.</returns>
        public static (string ListName, string CardName) GetPathSegments(string cardPath, char separator = '/')
        {
            string[] parts = cardPath.Split(new[] {separator}, 2, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
            {
                throw new ArgumentException($"The specified card path is wrong: '{cardPath}'.", nameof(cardPath));
            }
            return (parts[0], parts[1]);
        }

    }

}
