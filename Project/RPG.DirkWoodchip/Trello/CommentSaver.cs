﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using Articy.Api;
using JetBrains.Annotations;
using Manatee.Trello;

namespace RPG.DirkWoodchip.Trello
{
    /// <summary> The final step of saving parsed comments in articy:draft. </summary>
    public class CommentSaver
    {
        public const string TrelloRootId = "Ufo_Documents_Trello";
        public const string TrelloDialogueTemplate = "TrelloDialogue";

        readonly Color AcceptedColor = PluginData.ColorTable["Village"];
        readonly Color RejectedColor = PluginData.ColorTable["Royal"];
        readonly Color DefaultColor = PluginData.ColorTable["Blank"];
        public static readonly Emoji AcceptedEmoji = Emojis.WhiteCheckMark;
        public static readonly Emoji RejectedEmoji = Emojis.NegativeSquaredCrossMark;

        ApiSession Session { get; }
        CommentLoader Loader { get; }

        ObjectProxy TrelloFolder
        {
            get { return Session.GetObjectByTechName(TrelloRootId); }
        }

        /// <summary> Creates a new <see cref="CommentSaver"/> instance. </summary>
        /// <param name="session"> The current articy:draft session. </param>
        /// <param name="limit">The number of comments to download. The maximum *per card* is defined in <see cref="CommentLoader.TrelloApiLimit"/>.</param>
        public CommentSaver(ApiSession session, int limit = int.MaxValue)
        {
            Session = session;
            Loader = new CommentLoader(limit);
        }

        /// <summary> Updates a card document in Trello/List/[Card Document] location. </summary>
        /// <param name="document">A document which resides in a List folder under the defined <see cref="TrelloRootId"/>.</param>
        /// <param name="filter">What kind of comments to download.</param>
        public async void UpdateTrelloDocument(ObjectProxy document, CommentLoader.CommentFilter filter = CommentLoader.CommentFilter.Everything)
        {
            string listName = document.GetParent().GetDisplayName();
            string cardName = document.GetDisplayName();
            string cardPath = $"{listName}/{cardName}";

            var comments = await Loader.GetAllComments(new []{cardPath}, filter); // called synchronously
            var raws = CommentParser.FilterDialogues(comments, document);
            var dialogues = CommentParser.ParseDialogues(raws);

            foreach (Dialogue dialogue in dialogues.Reverse()) // most recent are at the top
            {
                // TODO: Update if not excluded
                ObjectProxy aDialogue = AddDocumentDialogue(document, dialogue);
                aDialogue.SetColor(!dialogue.rating.HasValue ? DefaultColor : 
                                           (dialogue.rating.Value ? AcceptedColor : RejectedColor));
                AttachDialogueActors(aDialogue, dialogue);
                AddDialogueEntries(aDialogue, dialogue.dialogueEntries);
            }

            System.Media.SystemSounds.Asterisk.Play(); // Play 'Finished' for each document
        }

        /// <summary> Adds a dialogue if it's not yet present. </summary>
        /// <param name="document">A document to add the dialogue to.</param>
        /// <param name="dialogue">A dialogue to create in the document.</param>
        ObjectProxy AddDocumentDialogue(ObjectProxy document, Dialogue dialogue)
        {
            string title = dialogue.TryGetFirstLabel(out string firstLabel)
                    ? firstLabel
                    : dialogue.ToString();
            string description = dialogue.creationDate.ToString("dd. MM. yyyy");
            ObjectProxy aDialogue = Session.CreateDocumentDialogue(document, HierarchyAdjustment.AsChild, title, description, TrelloDialogueTemplate);
            aDialogue.SetExternalId(dialogue.id);
            return aDialogue;
        }

        /// <summary> Fills a blank dialogue with dialogue fragments. </summary>
        /// <param name="aDialogue">A blank articy:draft dialogue.</param>
        /// <param name="dialogueEntries">Dialogue entries to add.</param>
        /// <exception cref="ArgumentException">The dialogue is null or not blank.</exception>
        void AddDialogueEntries(ObjectProxy aDialogue, IEnumerable<DialogueEntry> dialogueEntries)
        {
            if (aDialogue.GetChildren().Count != 0)
            {
                throw new ArgumentException("The dialogue already contains existing entries.", nameof(aDialogue));
            }

            foreach (DialogueEntry entry in dialogueEntries)
            {
                ObjectProxy aDialogueFragment = Session.CreateDialogueFragment(aDialogue, entry.RichTextLine);
                aDialogueFragment[ObjectPropertyNames.Speaker] = GetActorByName(entry.actor);
                aDialogueFragment[ObjectPropertyNames.MenuText] = entry.NestingMark;
                aDialogueFragment[ObjectPropertyNames.StageDirections] = entry.StageDirections;
            }
        }

        /// <summary> Adds dialogue actors to its reference strip. </summary>
        /// <param name="aDialogue">A blank articy:draft dialogue.</param>
        /// <param name="dialogue">A Trello dialogue to create in the document.</param>
        void AttachDialogueActors(ObjectProxy aDialogue, Dialogue dialogue)
        {
            var actorNames = dialogue.dialogueEntries.Select(entry => entry.actor).Distinct();
            var actors = actorNames.Select(GetActorByName).Where(actor => actor != null).Reverse();
            foreach (ObjectProxy actor in actors)
            {
                // Adds characters to the reference strip
                if (aDialogue.GetAttachments().Contains(actor)) continue;
                aDialogue.InsertAttachmentIntoStrip("Attachments", actor, 0);
            }
        }

        /// <summary> Retrieves an actor with a specified name. </summary>
        /// <param name="actorName">The name of the actor.</param>
        /// <returns>The actor entity.</returns>
        [CanBeNull]
        ObjectProxy GetActorByName(string actorName)
        {
            string actorQuery = $"SELECT * FROM Entities WHERE DisplayName == '{actorName.Replace("'", "''")}' LIMIT 1";
            ResultSet queryResults = Session.RunQuery(actorQuery);
            return queryResults.Rows.Count > 0 ? queryResults.Rows[0] : null;
        }

        /// <summary> Rates a Trello comment linked by a dialogue. </summary>
        /// <param name="dialogue"></param>
        /// <param name="rating"></param>
        public async void RateTrelloDialogue(ObjectProxy dialogue, bool? rating)
        {
            string id = dialogue.GetExternalId();

            Color color;
            Emoji emoji;
            switch (rating)
            {
                case true:
                    color = AcceptedColor;
                    emoji = AcceptedEmoji;
                    break;
                case false:
                    color = RejectedColor;
                    emoji = RejectedEmoji;
                    break;
                default:
                case null:
                    color = DefaultColor;
                    emoji = null;
                    break;
            }
            dialogue.SetColor(color);
            await Loader.RateComment(id, emoji);
        }
    }
}
