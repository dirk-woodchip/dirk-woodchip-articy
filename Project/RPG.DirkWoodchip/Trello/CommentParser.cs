﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Articy.Api;
using Manatee.Trello;

namespace RPG.DirkWoodchip.Trello
{
    /// <summary> Parsing comments loaded with <see cref="CommentLoader"/>. </summary>
    public static class CommentParser
    {

        const RegexOptions Options = RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.ExplicitCapture | RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled;
        static readonly Regex DialogueRegex = new Regex(@"^(\**?\[(?<label>.*?)\]\**?)? # optional label
                                                           (?<nesting>\s*?^[ ]*(0\.[ ])?) # is the dialogue line nested (spaces in front of it)?
                                                           \*\*(?<actor>.+?)\*\*:[ ](?<line>.*?) # the actor and spoken line
                                                           (\n|\z|\s*?---\s*?)(?=\s*(0\.[ ])?\*\*|\n|$) # possible dialogue line endings
                                                        ", Options); // **DIRK**: Hello.

        // TODO: Check nesting levels of a new piece of dialogue
        /*
0. **DIRK**: You're a jerk.
**ELLIE**: Oh, I know. But at least I'm self-aware.
Kyle, on the other hand, is too stubborn to notice I'm one of the select few people who actually care about him. But no, no. To him, being right is far more important than being nice. He'd rather bury himself deep inside that sulky hole he's fallen into in the past weeks– are you listening?

    0. **DIRK**: Yes…?
    0. **DIRK**: Of course not.

**ELLIE**: *[pierces Dirk with her look]*
**ELLIE**: I'm sorry. I needed to get it out.
**ELLIE**: I don't know if you noticed, but lately everybody has been so tense and touchy. Caravans are flowing through Morten but no-one buys anything, we frown at outsiders and townsfolk keep disappearing… this place just doesn't feel like home anymore. And everybody talks like something big is about to happen.  

0. **DIRK**: Are you scared?
**ELLIE**: I wouldn't be – if I had at least one person in life I can rely on.
         */
        const string ArticyTrelloFolder = "Ufo_Documents_Trello";

        /// <summary> Filters all actions to include only valid dialogues. </summary>
        public static IEnumerable<IAction> FilterDialogues(IEnumerable<IAction> comments, IEnumerable<string> excludeIds = null)
        {
            HashSet<string> excludeSet = excludeIds?.ToHashSet() ?? new HashSet<string>();
            return comments.Where(comment => DialogueRegex.IsMatch(comment.Data.Text) && !excludeSet.Contains(comment.Id));
        }

        /// <summary> Filters all actions to include only valid dialogues. </summary>
        /// <param name="comments"> Comments to filter. </param>
        /// <param name="document"> An articy:draft document already containing some of the processed documents.
        /// Their ID must be defined in <see cref="ObjectPropertyNames.ExternalId"/> to be taken into an account.</param>
        /// <exception cref="ArgumentException">The supposed document isn't actually a document.</exception>
        public static IEnumerable<IAction> FilterDialogues(IEnumerable<IAction> comments, ObjectProxy document)
        {
            if (document.ObjectType != ObjectType.Document)
            {
                throw new ArgumentException("The supposed document passed isn't really a document.");
            }
            // Get Trello dialogues already inside
            IEnumerable<string> externalIds = document.GetChildren()
                                                      .Where(child => child.ObjectType == ObjectType.Dialogue)
                                                      .Where(dialogue => dialogue.HasExternalId)
                                                      .Select(dialogue => dialogue.GetExternalId());
            return FilterDialogues(comments, externalIds);
        }

        /// <summary> Parses a dialogue into an object with groups. See <see cref="DialogueRegex"/> for available information. </summary>
        public static IEnumerable<Dialogue> ParseDialogues(IEnumerable<IAction> iDialogues)
        {
            // List of list of dialogue lines
            var dialogues = new List<Dialogue>();
            // For each specific dialogue
            foreach (IAction iDialogue in iDialogues)
            {
                // Reaction    
                Emoji[] emojis = iDialogue.Reactions.Select(r => r.Emoji).ToArray();
                bool? rating;
                if (emojis.Contains(CommentSaver.AcceptedEmoji)) rating = true;
                else if (emojis.Contains(CommentSaver.RejectedEmoji)) rating = false;
                else rating = null;

                // Find all dialogue lines
                string dialogueText = iDialogue.Data.Text;
                MatchCollection dialogueMatches = DialogueRegex.Matches(dialogueText);
                var entries = new List<DialogueEntry>();
                int previousNesting = 0;
                foreach (Match entry in dialogueMatches)
                {
                    var dialogueEntry = new DialogueEntry(entry, ref previousNesting);
                    entries.Add(dialogueEntry);
                }

                string owner = $"{iDialogue.Data.List.Name}/{iDialogue.Data.Card.Name}";
                var dialogue = new Dialogue(iDialogue.Id, iDialogue.CreationDate, owner, entries.ToArray(), rating);
                dialogues.Add(dialogue);
            }
            return dialogues;
        }

    }

}
