﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Manatee.Trello;
using RPG.DirkWoodchip.Trello;

namespace Test
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Trello loader example:");
            var trello = new CommentLoader(15);
            List<IAction> comments = await trello.GetAllComments(new[]
            {
                    "Ideas/References & Random II",
                    "Ideas/References & Random I"
            });

            await trello.RateComment("5c7dad5977e4c548ab517ade", Emojis.WhiteCheckMark);
            await trello.RateComment("5c7dad5977e4c548ab517ade", Emojis.NegativeSquaredCrossMark);

            var rawDialogues = CommentParser.FilterDialogues(comments);
            var dialogues = CommentParser.ParseDialogues(rawDialogues);

            var labelFull = dialogues.SelectMany(dialogue => dialogue.dialogueEntries.Skip(1))
                                     .Where(entry => !string.IsNullOrWhiteSpace(entry.label));

            await trello.RateComment("5c7dad5977e4c548ab517ade", null);
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
