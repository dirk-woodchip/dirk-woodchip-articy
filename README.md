An *articy:draft* plugin for easier linking of conversations in the style of the game, and turning dialogue drafts written on Trello into articy:draft documents.
